#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Reads in a default set of stopwords from a text file for all current users and all queries.
"""

import os

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from query.models import StopWord

class Command(BaseCommand):
	args = '<stopword_list...>'
	help = 'Reads in specified lists of stopwords'

	def handle(self, *args, **options):
		print 'Emptying table...'
		StopWord.objects.all().delete()
		users = User.objects.all()

		filenames = [] 
		for arg in args: 
			filenames.append(os.path.join(os.path.dirname(__file__), arg))

		for filename in filenames: 
			with open(filename) as f: 
				for line in f: 
					for user in users:
						StopWord.objects.create(word=line.strip(), user=user)
				print 'Stopwords from file %s added.' % filename
